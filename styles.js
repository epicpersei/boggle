import { StyleSheet, Dimensions } from 'react-native';
const {height, width} = Dimensions.get('window');
export default  StyleSheet.create({
    // MainScreen
    mainScreenLettersHolder: {
        backgroundColor: '#F5FCFF',
        flex: 1
    }, 
    lettersHolder: {
        flexDirection: 'row',
        flex: 0.5,
        height: 30,
        borderColor: 'green',
        borderWidth: 1,
        flexWrap: 'wrap'
    },
    wordAndActions :{
        flex: 0.5,
    },
    response : {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'left'
    },
    // LetterBox
    letterBoxContainer: {
        backgroundColor: 'black',
        borderWidth: 1,
        width: width / 6,
        height: width / 6,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    letter: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 30,
        textAlign: 'center'
    }
  });