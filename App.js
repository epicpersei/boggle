/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {SafeAreaView, View, Text, TouchableOpacity, Button, Alert} from 'react-native';
//components
import LetterBox from './components/letterBox';

//utils
import { randomLetter } from './utils'

import styles from './styles';

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      letters: false,
      word: '',
      addedWords: [],
      dictionary: []
    };
  }

  componentDidMount() {
    this.generateLetters();
    this.getDiectionary();
  }

  getDiectionary = () => {
    const urlDictionary = 'https://raw.githubusercontent.com/dwyl/english-words/master/words_dictionary.json';

    return fetch(urlDictionary)
				.then((response) => response.json())
				.then((responseJson) => {
          this.setState({
            dictionary: Object.keys(responseJson)
          });
			})
			.catch((error) => {
				console.log('fail');
				console.error(error);
			});
  }

  generateLetters = () => {
    let letters = [];
    for (i = 0; i< 25; i++) {
      letters.push(randomLetter());
    }
    this.setState({letters});
  }

  selectLetter = (letter)=> () => {
     const word = `${this.state.word}${letter}`;
     this.setState({word});
  }

  addWord = () => {
    if (this.state.word === '') {
      Alert.alert('You have no word entered');
      return false;
    }
    const addedWords = [...this.state.addedWords, this.state.word];
    this.setState({
      addedWords,
      word: ''
    });
  }

  clearWord = () => {
    this.setState({ word: '' });
  }

  checkWords = () => {
    const { addedWords, dictionary } = this.state;
    let correct = 0;
    let incorrect = 0;
    if (!addedWords) {
      Alert.alert('You must enter some words');
      return false;
    }
    addedWords.forEach(w => {
      if (dictionary.includes(w)) {
        correct = correct + 1;
      } else {
        incorrect = incorrect + 1;
      }
    });

    Alert.alert(`Correct: ${correct} / Incorrect: ${incorrect}`);

  }

  render() {

    const {letters, word, addedWords} = this.state;
    console.log(letters);

    return (
      <SafeAreaView style={styles.mainScreenLettersHolder}>
      <View style={styles.lettersHolder} >
        { letters && letters.map((l, index) =>
          <TouchableOpacity key={index} onPress={this.selectLetter(l)}>
            <LetterBox  letter={l} />
          </TouchableOpacity>
        )}
        </View>

        <View style={styles.wordAndActions}>
          <Text style={styles.response}>Word: {word} </Text>
          <Text style={styles.response}>Added Words: {addedWords.map((adword, index) => index === 0 ?
          adword : `, ${adword}`)} </Text>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Button
              onPress={this.addWord}
              title="Add Word"
              color="#841584"
            />
            <Button
              onPress={this.clearWord}
              title="Clear Word"
              color="#841584"
            />
          </View>
          <Button
            onPress={this.checkWords}
            title="Check Words"
            color="#841584"
          />
        </View>
      </SafeAreaView>
    );
  }
}
