export const randomLetter =  () => {
    const letter = String.fromCharCode(
        Math.floor(Math.random() * 26) + 97
    );
    return letter === 'q' ? 'qu' : letter; 
}
