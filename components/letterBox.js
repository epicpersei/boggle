import React, {Component} from 'react';
import propTypes from 'prop-types';
import {
  Text,
  View,
} from 'react-native';
import styles from '../styles';

class LetterBox extends Component {

  render() {
    const { letter } = this.props;
    return (      
      <View style={styles.letterBoxContainer}> 
       <Text style={styles.letter}>{letter}</Text>
      </View>
    );
  }
}

LetterBox.propTypes = {
  letter: propTypes.string.isRequired
}

export default LetterBox;
